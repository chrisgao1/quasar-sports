import Mock from "mockjs";

const players = [
  Mock.mock({
    name: "Stephen Curry",
    short: "curry",
    year: 2022,
    team: "GSW",
    league: "nba",
    score: "@float(23, 35, 2, 2)",
    assist: "@float(5, 12, 2, 2)",
    fieldper: "@float(0.38, 0.6, 4, 4)",
    appearance: "@integer(50,82)",
    time: "@float(30, 46, 2, 2)",
  }),
  Mock.mock({
    name: "Stephen Curry",
    short: "curry",
    year: 2021,
    team: "GSW",
    league: "nba",
    score: "@float(23, 35, 2, 2)",
    assist: "@float(5, 12, 2, 2)",
    fieldper: "@float(0.38, 0.6, 4, 4)",
    appearance: "@integer(50,82)",
    time: "@float(30, 46, 2, 2)",
  }),
  Mock.mock({
    name: "Stephen Curry",
    short: "curry",
    year: 2020,
    team: "GSW",
    league: "nba",
    score: "@float(23, 35, 2, 2)",
    assist: "@float(5, 12, 2, 2)",
    fieldper: "@float(0.38, 0.6, 4, 4)",
    appearance: "@integer(50,82)",
    time: "@float(30, 46, 2, 2)",
  }),
];

export default [
  {
    url: "/api/players/data",
    type: "get",
    response: config => {
      let dataList = players;
      if (config.query.short) {
        dataList = dataList.filter(s => s.short === config.query.short);
      }
      return {
        code: 20000,
        data: dataList,
        total: dataList.length,
      };
    },
  },
];
