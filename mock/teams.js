import Mock from "mockjs";

const teams = [
  Mock.mock({
    id: "@id",
    name: "Chicago Bulls",
    short: "CHI",
    league: "nba",
    icon: "/icons/team/nba/bulls.jpeg",
    city: "Chicaco,IL",
    country: "USA",
  }),
  Mock.mock({
    id: "@id",
    name: "Los Angelas Lakers",
    short: "LAL",
    league: "nba",
    icon: "/icons/team/nba/lakers.jpeg",
    city: "LA,CA",
    country: "USA",
  }),
  Mock.mock({
    id: "@id",
    name: "Golden State Warriors",
    short: "GSW",
    league: "nba",
    icon: "/icons/team/nba/warriors.jpeg",
    city: "San Francisco,CA",
    country: "USA",
  }),
  Mock.mock({
    id: "@id",
    name: "Houston Rockets",
    short: "HOU",
    league: "nba",
    icon: "/icons/team/nba/rockets.jpeg",
    city: "Houston,TX",
    country: "USA",
  }),
  Mock.mock({
    id: "@id",
    name: "Manchester United",
    short: "MU",
    league: "premier",
    icon: "/icons/team/soccer/mu.png",
    city: "Manchester",
    country: "England",
  }),
  Mock.mock({
    id: "@id",
    name: "Manchester City",
    short: "MC",
    league: "premier",
    icon: "/icons/team/soccer/mc.png",
    city: "Manchester",
    country: "England",
  }),
  Mock.mock({
    id: "@id",
    name: "Real Madrid",
    short: "RM",
    league: "champions",
    icon: "/icons/team/soccer/rm.png",
    city: "Madrid",
    country: "Spain",
  }),
  Mock.mock({
    id: "@id",
    name: "FC Barcelona",
    short: "BC",
    league: "champions",
    icon: "/icons/team/soccer/bc.png",
    city: "Barcelona",
    country: "Spain",
  }),
  Mock.mock({
    id: "@id",
    name: "Paris Saint German",
    short: "PSG",
    league: "champions",
    icon: "/icons/team/soccer/psg.png",
    city: "Paris",
    country: "France",
  }),
];

export default [
  {
    url: "/api/teams/list",
    type: "get",
    response: config => {
      let dataList = teams;
      if (config.query.league) {
        dataList = dataList.filter(s => s.league === config.query.league);
      } else if (config.query.id) {
        dataList = dataList.filter(s => s.id === config.query.id);
      }
      return {
        code: 20000,
        data: dataList,
        total: dataList.length,
      };
    },
  },
];
