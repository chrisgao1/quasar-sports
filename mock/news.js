import Mock from "mockjs";

const { newsList } = Mock.mock({
  "newsList|3": [
    {
      id: "@id",
      timestamp: +Mock.Random.date("T"),
      author: "@first",
      reviewer: "@first",
      title: "@title(5, 10)",
      content_short: "@sentence(15, 30)",
      content: "@paragraph()",
      importance: "@integer(1, 3)",
      "type|1": ["nba", "premier", "chamipions"],
      "status|1": ["published", "draft"],
      display_time: "@datetime",
      comment_disabled: false,
      pageviews: "@integer(300, 5000)",
      image_uri: `/icons/news/news@increment().jpg`,
    },
  ],
});

export default [
  {
    url: "/api/news/list",
    type: "get",
    response: config => {
      let dataList = newsList;
      if (config.query.id) {
        dataList = dataList.filter(s => s.id === config.query.id);
      }
      if (config.query.type) {
        dataList = dataList.filter(s => s.id === config.query.type);
      }
      return {
        code: 20000,
        data: dataList,
        total: dataList.length,
      };
    },
  },
];
