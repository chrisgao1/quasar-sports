import Mock from "mockjs";

const tokens = {
  admin: {
    token: "admin-token",
    password: "admin",
  },
  editor: {
    token: "editor-token",
    password: "editor",
  },
};

const users = {
  "admin-token": {
    roles: ["admin"],
    introduction: "Super Admin",
    avatar:
      "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
    name: "Admin",
  },
  "editor-token": {
    roles: ["editor"],
    introduction: "Editor",
    avatar:
      "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
    name: "Editor",
  },
};

export default [
  {
    url: "/api/user/info",
    type: "post",
    response: config => {
      const { token } = config.body;
      const info = users[token];

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: "Login failed! User information is not found",
        };
      }

      return {
        code: 20000,
        data: info,
      };
    },
  },
  {
    url: "/api/user/login",
    type: "post",
    response: config => {
      const { username, password } = config.body;
      const token = tokens[username];

      // mock error
      if (!token) {
        return {
          code: 60204,
          message: "Invalid Credential",
        };
      }
      if (token["password"] !== password) {
        return {
          code: 60207,
          message: "Incorrect Password",
        };
      }

      return {
        code: 20000,
        data: token,
      };
    },
  },
];
