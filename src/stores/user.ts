import { UserInfo } from "models/user";
import { doSubmit, doPost } from "utils/api";
import { defineStore } from "pinia";
import { getToken, setToken, removeToken } from "utils/auth";
import { usePermissionStore } from "./permission";
import { pinia } from "./index";
import * as actions from "./action-types";

const store = usePermissionStore(pinia);

export const useUserStore = defineStore("user", {
  state: () => ({
    token: getToken(),
    name: "",
    avatar: "",
    introduction: "",
    roles: [],
  }),

  getters: {},

  actions: {
    login(userInfo: UserInfo) {
      const { username, password } = userInfo;
      return new Promise<void | string>((resolve, reject) => {
        doPost("/user/login", {
          username: username.trim(),
          password: password,
        })
          .then(response => {
            const { data, message, code } = response;
            if (code === 20000) {
              this.getInfo(data.token)
                .then(() => {
                  setToken(data.token);
                  resolve();
                })
                .catch(error => {
                  reject(error);
                });
            } else {
              reject(message);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getInfo(token: string) {
      return new Promise<any>((resolve, reject) => {
        doPost("/user/info", { token })
          .then(response => {
            const { data, message, code } = response;
            if (code === 20000) {
              const { roles, name, avatar, introduction } = data;
              this.SET_TOKEN(token);
              this.SET_ROLES(roles);
              this.SET_NAME(name);
              this.SET_AVATAR(avatar);
              this.SET_INTRODUCTION(introduction);
              resolve(data);
            } else {
              reject(message);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    logout() {
      removeToken();
      this.$reset();
      store.resetRoutes();
    },
    [actions.SET_TOKEN](token: string) {
      this.$patch(state => {
        state.token = token;
      });
    },
    [actions.SET_ROLES](roles: any) {
      this.$patch(state => {
        state.roles = roles;
      });
    },
    [actions.SET_INTRODUCTION](introduction: any) {
      this.$patch(state => {
        state.introduction = introduction;
      });
    },
    [actions.SET_NAME](name: any) {
      this.$patch(state => {
        state.name = name;
      });
    },
    [actions.SET_AVATAR](avatar: any) {
      this.$patch(state => {
        state.avatar = avatar;
      });
    },
  },
});
