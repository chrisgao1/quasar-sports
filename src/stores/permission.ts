import { Route } from "models/route";
import { defineStore } from "pinia";
import { RouteLocation, RouteRecordRaw } from "vue-router";
import { Router } from "router/index";
import { asyncRoutes } from "router/permissions";

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
export function hasPermission(
  roles: Array<string>,
  route: RouteRecordRaw | RouteLocation
) {
  if (route.meta?.roles) {
    return roles.some(role =>
      (route.meta?.roles as Array<string>).includes(role)
    );
  } else {
    return true;
  }
}

export const usePermissionStore = defineStore("permission", {
  state: () => ({
    routes: Array<Route>(),
  }),

  getters: {
    menus(state) {
      return state.routes.filter(route => route.menu);
    },
  },

  actions: {
    /**
     * generate Routes
     * @param roles
     * @returns Routes
     */
    generateRoutes(roles: Array<any>) {
      return new Promise(resolve => {
        let accessedRoutes;
        // 超管可以访问所有页面
        if (roles.includes("admin")) {
          accessedRoutes = asyncRoutes || [];
        } else {
          accessedRoutes = this.filterAsyncRoutes(asyncRoutes, roles);
        }
        this.addRoutes(accessedRoutes);
        resolve(accessedRoutes);
      });
    },
    /**
     * Filter asynchronous routing tables by recursion
     * @param routes asyncRoutes
     * @param roles
     */
    filterAsyncRoutes(routes: RouteRecordRaw[], roles: Array<string>) {
      const res: RouteRecordRaw[] = [];

      routes.forEach(route => {
        const tmp = { ...route };
        if (hasPermission(roles, tmp)) {
          if (tmp.children && tmp.children.length > 0) {
            tmp.children = this.filterAsyncRoutes(tmp.children, roles);
          }
          res.push(tmp);
        }
      });

      return res;
    },
    addRoutes(addRoutes: RouteRecordRaw[]) {
      addRoutes.forEach(route => {
        // Router.addRoute(route);
        this.addRoute(route);
      });
    },
    addRoute(route: RouteRecordRaw) {
      this.generateRoute(route);
    },
    generateRoute(route: RouteRecordRaw) {
      const addedRoute: Route = {
        id: route.meta?.id as number,
        name: route.name as string,
        path: route.path,
        to: route.path,
        link: route.path,
        roles: route.meta?.roles as string[],
        label: route.meta?.label as string,
        title: route.meta?.label as string,
        caption: route.meta?.caption as string,
        icon: route.meta?.icon as string,
        menu: route.meta?.menu as boolean,
        keepAlive: route.meta?.keepAlive as boolean,
      };
      this.routes.push(addedRoute);
      if (route.children?.length) {
        for (const r of route.children) {
          this.generateRoute(r);
        }
      }
    },
    resetRoutes() {
      this.$reset();
    },
  },
});
