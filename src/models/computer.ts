import { IComputer } from "./icomputer";
export class Computer implements IComputer {
  private _name: string;

  private _category: string;

  constructor(name: string, category: string) {
    this._name = name;
    this._category = category;
  }

  play(params1: any, params2?: any): string {
    return params1.toString() + params2?.toString();
  }

  get name() {
    return this._name;
  }
  set name(name: string) {
    this._name = name;
  }

  get category() {
    return this._category;
  }
  set category(category: string) {
    this._category = category;
  }
}
