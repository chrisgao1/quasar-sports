export declare interface News {
  id: string | number;
  timestamp: string;
  author: string;
  reviewer: string;
  title: string;
  content_short: string;
  content: string;
  importance: number;
  type: string;
  status: string;
  display_time: string;
  comment_disabled: boolean;
  pageviews: number;
  image_uri: string;
}
