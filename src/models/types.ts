import { InjectionKey } from "vue";

export type UpdateTitle = (val: string) => void;

export const updateTitleKey: InjectionKey<UpdateTitle> = Symbol();
