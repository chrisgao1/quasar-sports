export declare interface IComputer {
  play(params: string): string;
  play(params: number, params1: string): string;
}
