export declare interface Player {
  id: number | string;
  name: string;
  nickname: string;
  icon: string;
  team: string | number;
  league: string;
  number: number;
  nation: string;
  age: number;
  position: string;
  rate: number;
  status: string;
  [propName: string]: any;
}
