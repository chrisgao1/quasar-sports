export declare interface Route {
  id?: number;
  name?: string;
  path: string;
  link?: string;
  to?: string;
  label?: string;
  title?: string;
  caption?: string;
  roles?: string[];
  icon?: string;
  menu?: boolean;
  keepAlive?: boolean;
  children?: Route[];
  [propName: string]: any;
}
