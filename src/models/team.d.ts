export declare interface Team {
  id: number | string;
  name: string;
  short: string;
  icon: string;
  league: number;
  country: string;
  city?: string;
  [propName: string]: any;
}
