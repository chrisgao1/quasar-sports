import { useUserStore } from "stores/user";
import { App, DirectiveBinding } from "vue";
import { pinia } from "stores/index";

const store = useUserStore(pinia);

export default {
  install(app: App, options: any) {
    // 直接export default function install(app: App, options: any) { ... } 也可以定义指令
    // 提供 name 全局属性，name 的值取决于插件可选配置
    // 提供 v-focus 自定义指令
    app.directive("permission", permissionDirective);
  },
};

function checkPermission(el: HTMLElement, binding: DirectiveBinding) {
  const { value } = binding;
  const roles = store.roles;
  let hasPermission = false;
  if (value) {
    if (((value as string[]) && value.length > 0) || value instanceof String) {
      const res: string[] = [];
      const permissionRoles = res.concat(value);

      hasPermission = roles.some(role => {
        return permissionRoles.includes(role);
      });
    }
  }
  if (!hasPermission) {
    el.setAttribute("disabled", "true");
  }
}

const permissionDirective = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    checkPermission(el, binding);
  },
  updated(el: HTMLElement, binding: DirectiveBinding) {
    checkPermission(el, binding);
  },
};
