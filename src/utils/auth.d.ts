export declare function getToken(): string;

export declare function setToken(token: string): string;

export declare function removeToken(): void;
