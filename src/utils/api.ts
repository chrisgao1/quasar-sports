import request from "utils/request";
import qs from "qs";

export function doSubmit(url: string, params: any = {}): Promise<any> {
  return request({
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    url: url,
    method: "post",
    data: qs.stringify(params),
  });
}

export function doPost(url: string, data: any = {}): Promise<any> {
  return request({
    url: url,
    method: "post",
    data: data,
  });
}

export function doGet(url: string, query?: any): Promise<any> {
  const searchParams = new URLSearchParams();
  let requestUrl = url;
  if (query && Object.keys(query).length) {
    for (const key in query) {
      searchParams.set(key, String(query[key]));
    }
    requestUrl = url
      .concat(url.endsWith("?") ? "" : "?")
      .concat(searchParams.toString());
  }
  return request({
    url: requestUrl,
    method: "get",
    // params: query,
  });
}
