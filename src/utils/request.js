import axios from "axios";

// 创建axios实例
const service = axios.create({
  // 在请求地址前面加上baseURL
  baseURL: import.meta.env.VITE_AXIOS_BASE_URL,
  // 当发送跨域请求时携带cookie
  // withCredentials: true,
  timeout: 10000,
});

// 请求拦截
service.interceptors.request.use(
  config => {
    // 请求发送之前可以做预处理
    // if (store.token) {
    // // 自定义令牌的字段名为X-Token，根据咱们后台再做修改
    // config.headers["X-Token"] = store.token;
    // }
    return config;
  },
  error => {
    // 请求错误的统一处理
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  config => {
    return config.data;
  },
  error => {
    // 请求错误的统一处理
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

export default service;
