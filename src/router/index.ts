import { route } from "quasar/wrappers";
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory,
  RouteRecordRaw,
} from "vue-router";

import routes from "./routes";
import { asyncRoutes } from "./permissions";

import { pinia } from "@/stores";
import { useUserStore } from "stores/user";
import { usePermissionStore, hasPermission } from "stores/permission";
import { getToken } from "utils/auth";

const userStore = useUserStore(pinia);
const permissionStore = usePermissionStore(pinia);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

const createHistory = process.env.SERVER
  ? createMemoryHistory
  : process.env.VUE_ROUTER_MODE === "history"
  ? createWebHistory
  : createWebHashHistory;

export const Router = createRouter({
  scrollBehavior: () => ({ left: 0, top: 0 }),
  routes,

  // Leave this as is and make changes in quasar.conf.js instead!
  // quasar.conf.js -> build -> vueRouterMode
  // quasar.conf.js -> build -> publicPath
  history: createHistory(process.env.VUE_ROUTER_BASE),
});

asyncRoutes.forEach(route => {
  Router.addRoute(route);
  // permissionStore.addRoute(route);
});

const whiteList = routes.reduce((p: string[], c) => {
  const temp: RouteRecordRaw[] = [];
  temp.push(c);
  while (temp.length > 0) {
    const children = temp[0].children;
    if (children && children.length > 0) {
      for (const cc of children) {
        temp.push(cc);
      }
    }
    const cur = temp.shift();
    if (cur?.name) {
      p.push(cur.name as string);
    }
  }

  return p;
}, []);

Router.beforeEach(async (to, from, next) => {
  const token = getToken();
  const matched: string[] = to.matched.map(m => m.path);
  if (matched.includes("/:catchAll(.*)*")) {
    next();
  } else if (token) {
    const { roles } = (await userStore.getInfo(token)) as {
      roles: Array<string>;
    };
    if (permissionStore.routes?.length > 0) {
      const ids: number[] = permissionStore.routes.map(m => m.id as number);
      if (
        !ids.includes(to.meta?.id as number) ||
        (to.redirectedFrom &&
          !hasPermission(roles, to.redirectedFrom) &&
          whiteList.indexOf(to.name as string) === -1)
      ) {
        next("/noPermisson");
      } else {
        next();
      }
    } else {
      try {
        await permissionStore.generateRoutes(roles);

        // hack method to ensure that addRoutes is complete
        // set the replace: true, so the navigation will not leave a history record
        next({ ...to, replace: true });
      } catch (error) {
        next(`/login?redirect=${to.fullPath}`);
      }
    }
  } else {
    if (whiteList.indexOf(to.name as string) !== -1) {
      // in the free login whitelist, go directly
      next();
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.fullPath}`);
    }
  }
});

export default route(function (/* { store, ssrContext } */) {
  return Router;
});
