import { RouteRecordRaw } from "vue-router";

const routes: RouteRecordRaw[] = [
  { path: "/login", name: "login", component: () => import("pages/Login.vue") },
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: "index",
        component: () => import("pages/Home.vue"),
        meta: {
          dashBoard: true,
        },
      },
      {
        path: "home",
        name: "home",
        component: () => import("pages/Home.vue"),
        meta: {
          dashBoard: true,
        },
      },
    ],
  },
  {
    name: "noPermisson",
    path: "/noPermisson",
    component: () => import("pages/NoPermission.vue"),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
