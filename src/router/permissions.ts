import { RouteRecordRaw } from "vue-router";

export const asyncRoutes: RouteRecordRaw[] = [
  {
    path: "/nba",
    name: "NBA",
    component: () => import("layouts/MainLayout.vue"),
    redirect: "/teams/list/nba",
    meta: {
      label: "nba",
      caption: "NBA",
      icon: "/icons/league/nba.png",
      menu: true,
    },
    children: [],
  },
  {
    path: "/premier",
    name: "Premier",
    component: () => import("layouts/MainLayout.vue"),
    redirect: "/teams/list/premier",
    meta: {
      label: "premier",
      caption: "Premier League",
      icon: "/icons/league/premier.png",
      menu: true,
    },
    children: [],
  },
  {
    path: "/champions",
    name: "Champions",
    component: () => import("layouts/MainLayout.vue"),
    redirect: "/teams/list/champions",
    meta: {
      label: "chamipions",
      caption: "Champions League",
      icon: "/icons/league/champions.png",
      menu: true,
      roles: ["admin"],
    },
    children: [],
  },
  {
    path: "/teams",
    component: () => import("layouts/MainLayout.vue"),
    redirect: "/teams/list",
    meta: {
      title: "球队",
    },
    children: [
      {
        name: "teamList",
        path: "list/:league?",
        component: () => import("pages/team/list.vue"),
        meta: {
          id: 1,
          title: "球队列表",
          keepAlive: true,
        },
      },
      {
        path: "detail/:id",
        component: () => import("pages/team/detail.vue"),
        meta: {
          id: 2,
          title: "球队信息",
        },
      },
    ],
  },
  {
    path: "/players",
    component: () => import("layouts/MainLayout.vue"),
    redirect: "/players/list",
    meta: {
      title: "球员",
    },
    children: [
      {
        path: "list/:league?/:team?",
        component: () => import("pages/player/list.vue"),
        meta: {
          id: 3,
          title: "球员列表",
        },
      },
      {
        path: "detail/:id",
        component: () => import("pages/player/detail.vue"),
        meta: {
          id: 4,
          title: "球员信息",
          icon: "star",
          roles: ["admin"],
        },
      },
    ],
  },
];
