const DotEnv = require("dotenv");
const parsed = DotEnv.config({
  path: `.env`,
}).parsed;
const parsedEnv = DotEnv.config({
  path: `.env.${process.env.NODE_ENV}`,
}).parsed;

module.exports = function () {
  return Object.assign(parsed || {}, parsedEnv || {});
};
