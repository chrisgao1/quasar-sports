import { createProdMockServer } from "vite-plugin-mock/es/createProdMockServer";
const modules = import.meta.globEager("/mock/*.js");

export function setupProdMockServer() {
  let mockList = [];
  for (let key in modules) {
    mockList = mockList.concat(...modules[key].default);
  }
  createProdMockServer([...mockList]);
}
