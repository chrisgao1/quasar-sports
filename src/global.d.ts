import { AxiosInstance } from "axios";

// 声明要扩充@vue/runtime-core包的声明.
// 这里扩充"ComponentCustomProperties"接口, 因为他是vue3中实例的属性的类型.
declare module "@vue/runtime-core" {
  // 给`this.$http`提供类型
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
    $resolve(...paths: string[]): string;
    $resolvePath(...paths: string[]): string;
    $resolveImgPath(...paths: string[]): string;
    $nvl(val: any, fallback: string): any;
    $choose(val: any, expected: any, fallback: string): any;
  }
}
