import { boot } from "quasar/wrappers";

import axios from "utils/request";
import { join } from "path";
import { setupProdMockServer } from "config/mockProdServer";

if (
  process.env.NODE_ENV === "production" &&
  import.meta.env.VITE_MOCK_ENABLED === "true"
) {
  setupProdMockServer();
}

import permission from "directives/permission";

import * as echarts from "echarts/core";
import { BarChart, PieChart, LineChart, RadarChart } from "echarts/charts";

import {
  GridComponent,
  TooltipComponent,
  TitleComponent,
  LegendComponent,
  AxisPointerComponent,
  PolarComponent,
} from "echarts/components";
import { CanvasRenderer } from "echarts/renderers";

echarts.use([
  LineChart,
  PieChart,
  CanvasRenderer,
  BarChart,
  RadarChart,
  GridComponent,
  TooltipComponent,
  TitleComponent,
  LegendComponent,
  AxisPointerComponent,
  PolarComponent,
]);

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({ app }) => {
  // something to do
  app.use(permission);
  app.config.globalProperties.$navTitle = "Quasar Sports";
  app.config.globalProperties.$axios = axios;
  app.config.globalProperties.$resolve = function (...paths: string[]) {
    return join(...paths);
  };
  app.config.globalProperties.$resolvePath = function (...paths: string[]) {
    return join(import.meta.env.VITE_BASE_URL, join(...paths));
  };
  app.config.globalProperties.$resolveImgPath = function (...paths: string[]) {
    return join("img:", import.meta.env.VITE_BASE_URL, join(...paths));
  };
  app.config.globalProperties.$nvl = function (val: any, fallback = "all") {
    return val || fallback;
  };
  app.config.globalProperties.$choose = function (
    val: any,
    expected: any,
    fallback = "all"
  ) {
    return val ? expected : fallback;
  };
});
