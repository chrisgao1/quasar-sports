import { doGet, doPost } from "utils/api";

export function list(team?: string, params: any = {}) {
  if (team) {
    return doGet(`/players/list/${team}`, params);
  }
  return doGet("/players/list", params);
}

export function data(params?: any) {
  if (params) {
    return doGet(`/players/data`, params);
  }
  return doGet("/players/data");
}
