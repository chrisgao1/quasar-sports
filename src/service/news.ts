import { doGet, doPost } from "utils/api";

export function query(params?: any) {
  if (params) {
    return doGet(`/news/list`, params);
  }
  return doGet("/news/list");
}
