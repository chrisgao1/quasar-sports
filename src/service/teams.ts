import { doGet, doPost } from "utils/api";

export function listByLeague(league?: string) {
  if (league) {
    return doGet("/teams/list", { league });
  }
  return doGet("/teams/list");
}

export function listById(id?: any) {
  if (id) {
    return doGet("/teams/list", { id });
  }
  return doGet("/teams/list");
}
