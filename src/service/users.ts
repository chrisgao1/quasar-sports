import { doGet, doPost, doSubmit } from "utils/api";

export function login(params: any) {
  return doPost("/user/login", params);
}
